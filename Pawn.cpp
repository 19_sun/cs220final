#include "Pawn.h"
#include <stdlib.h>
#include <cmath>

using std::pair;
//legal capture shape for pawn

bool Pawn::legal_capture_shape(pair<char,char>start, pair<char,char> end)const {
  if(abs(start.first - end.first) != 1) {
    return false;
  }//captures when only moving horizontally, by 1 column
  //checking vertical moves
  //if black moving down if white moving up
  if(this->is_white()) {
    if((end.second - start.second) != 1) {
      return false;
    }
  }
  else {
    if ((start.second - end.second) != 1) {
    return false;
    }
  }
  return true;
}
