// Knight.cpp                                                                                                                    

#include <iostream>
#include <string>
#include "Knight.h"
#include "Piece.h"
#include <cmath>

using std::string;

bool Knight::legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const
  {
	  //Knight only move 1 box and 2 box in the other direction
	  if((std::abs(start.first - end.first) > 1) ||
	     (std::abs(start.second - end.second) > 1)) {
	    return false;
	  }
	  // if King is not moving
	  else if ((start.first - end.first == 0) && (start.second - end.second == 0)){
	    return false;
	  }
	  return true;
	}
bool Knight::legal_capture_shape( std::pair< char , char > start , std::pair< char , char > end ) const {
  return Knight::legal_move_shape(start,end);
}
