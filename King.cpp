#include "King.h"

using std::pair;

// returns legal_move_shape function as defined in King.h 
bool King::legal_capture_shape(pair< char , char > start , pair< char , char > end) const {
  return King::legal_move_shape(start, end);
}
