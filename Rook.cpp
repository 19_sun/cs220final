#include "Rook.h"

using std::pair;

// legal_move_shape and legal_capture_shape checks for the same movement
bool Rook::legal_capture_shape(pair< char , char > start , pair< char , char > end) const {
  return Rook::legal_move_shape(start, end);
}
