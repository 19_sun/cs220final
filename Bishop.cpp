#include "Bishop.h"

using std::pair;

// legal_capture_shape function using the return value from move shape
bool Bishop::legal_capture_shape(pair< char , char > start , pair< char , char > end) const {
  return Bishop::legal_move_shape(start, end);
}
