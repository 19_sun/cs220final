#ifndef PAWN_H
#define PAWN_H
#include "Piece.h"
#include<stdlib.h>
class Pawn : public Piece
{
public:

 
  bool legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const
	{
	  	  if (start.first != end.first) {
	    return false;
	  }
	  if (((start.second == '2') && (is_white())) || ((start.second == '7') && (!is_white()))) {
	    if (is_white()) {
	      if (((end.second - start.second) > 2) || ((end.second - start.second) < 1)) {
		return false;
	      }
	    }
	    else {
	      if (((start.second - end.second) > 2) || ((start.second - end.second) < 1)) {
		return false;
	      }
	    }
	  }
	  else {
	    if (is_white()) {
	      if ((end.second - start.second) != 1) {
		return false;
	      }
	    }
	    else {
	      if ((start.second - end.second) != 1) {
		return false;
	      }
	    }
	  }
	  return true;

	}
  bool legal_capture_shape(std::pair<char,char>start, std::pair<char,char> end)const;

	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii( void ) const { return is_white() ? 'P' : 'p'; }

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	Pawn( bool is_white ) : Piece( is_white ) {}

	friend Piece* create_piece( char );
};

#endif // PAWN_H
