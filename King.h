// King.h

#ifndef KING_H
#define KING_H

#include "Piece.h"
#include <cmath>

class King : public Piece
{
public:


        // King only moves one box
        bool legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const
	{
	  //No moving more than one box
	  if((std::abs(start.first - end.first) > 1) ||
	     (std::abs(start.second - end.second) > 1)) {
	    return false;
	  }
	  // can not stay in original position
	  else if ((start.first - end.first == 0) && (start.second - end.second == 0)){
	    return false;
	  }
	  return true;
	}

	bool legal_capture_shape( std::pair< char , char > start , std::pair< char , char > end ) const;

	
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii( void ) const { return is_white() ? 'K' : 'k'; }

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	King( bool is_white ) : Piece( is_white ) {}

	friend Piece* create_piece( char );
};

#endif // KING_H
