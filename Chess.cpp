#include <cstdlib>
#include "Chess.h"

using std::string;


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Chess::Chess( void ) : _turn_white( true )
{
	// Add the pawns
	for( int i=0 ; i<8 ; i++ )
	{
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+1 ) , 'P' );
		_board.add_piece( std::pair< char , char >( 'A'+i , '1'+6 ) , 'p' );
	}

	// Add the rooks
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+0 ) , 'R' );
	_board.add_piece( std::pair< char , char >( 'A'+0 , '1'+7 ) , 'r' );
	_board.add_piece( std::pair< char , char >( 'A'+7 , '1'+7 ) , 'r' );

	// Add the knights
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+0 ) , 'N' );
	_board.add_piece( std::pair< char , char >( 'A'+1 , '1'+7 ) , 'n' );
	_board.add_piece( std::pair< char , char >( 'A'+6 , '1'+7 ) , 'n' );

	// Add the bishops
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+0 ) , 'B' );
	_board.add_piece( std::pair< char , char >( 'A'+2 , '1'+7 ) , 'b' );
	_board.add_piece( std::pair< char , char >( 'A'+5 , '1'+7 ) , 'b' );

	// Add the kings and queens
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+0 ) , 'Q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+0 ) , 'K' );
	_board.add_piece( std::pair< char , char >( 'A'+3 , '1'+7 ) , 'q' );
	_board.add_piece( std::pair< char , char >( 'A'+4 , '1'+7 ) , 'k' );
}

//creating a deep copy function to take board type as an input
//orig is the board we want to copy
//p_c, p_r are counters for piece
Board Chess::copy_board(const Board& orig) const {
  Board new_board;
  for(char p_c = 'A'; p_c <= 'H'; p_c++) {
    for(char p_r = '1'; p_r <= '8'; p_r++) {
      if(orig(std::pair<char, char>(p_c, p_r)) != NULL) {
        new_board.add_piece(std::pair<char, char>(p_c, p_r), orig(std::pair<char, char>(p_c, p_r))->to_ascii());
      }
    }   
  }
  return new_board;
}

// attempts to make a move
// if the move is successful change side
bool Chess::make_move( std::pair< char , char > start , std::pair< char , char > end )
{
  // Checks if the starting position has piece
  if (_board(start) == NULL) {
    return false;
  }

  // Storing the ending value
  char orig_end = ' ';
  if (_board(end) != NULL) {
    orig_end = _board(end)->to_ascii();
  }


  // Checking the boundaries of the board
  if (end.first < 'A' || end.first > 'H' || end.second < '1' || end.second > '8') {
    return false;
  }

  // checking the legal path
  // sometimes with only one check, it does not return false
  if (_board(end) == NULL) {
    if (_board(start)->legal_move_shape(start, end) == false) {
      return false;
    }
  }
  else {
    if (_board(start)->legal_capture_shape(start, end) == false) {
      return false;
    }
  }
  // result indicates the piece 
  bool result = _board.add_piece(end, _board(start)->to_ascii());
  _board.remove_piece(start);

  // Checks to see if after the move that person is in check; makes them do something that won't put them in check
  if(_board(end) != NULL && _board(start) == NULL && in_check(_turn_white, _board)) {
    _board.add_piece(start, _board(end)->to_ascii());
    if(orig_end == ' ') {
      _board.remove_piece(end);
    }
    else {
      _board.remove_piece(end);
      _board.add_piece(end, orig_end);
    }
    return false;
  }

  // Promotion of p or P to q or Q according to the sides
  if(_board(end) != NULL && _board(end)->to_ascii() == 'P' && end.second == '8') {
    _board.remove_piece(end);
    result = _board.add_piece(end, 'Q');
  }
  if(_board(end) != NULL && _board(end)->to_ascii() == 'p' && end.second == '1') {
    _board.remove_piece(end);
    result = _board.add_piece(end, 'q');
  }
  
  _turn_white = !_turn_white;
  return result;
}

// a very straight forward but complex way of checking in_check
bool Chess::in_check( bool white ) const
{
  //white
  if (white) {
    //looping through the entire board to find all the "k"s
    char k_col, k_row;
    for (char k_i = 'A'; k_i <= 'H'; k_i++) {
      for (char k_j = '1'; k_j <= '8'; k_j++) {
	if (_board(std::pair<char, char>(k_i, k_j)) != NULL && (_board(std::pair<char, char>(k_i, k_j))->to_ascii() == 'K')) {
	  k_col = k_i;
	  k_row = k_j;
	}
      }
    }
    //check the legal paths for the opponent side
    //return true if any of the chess is able to capture
    for (char p_i = 'A'; p_i <= 'H'; p_i++) {
      for (char p_j = '1'; p_j <= '8'; p_j++) {
	if ((_board(std::pair<char, char>(p_i, p_j)) != NULL) && (_board(std::pair<char, char>(p_i, p_j))->is_white() != white)) {
	  const Piece* start_piece = _board(std::pair<char, char>(p_i, p_j));
	  if ((start_piece->legal_capture_shape(std::pair<char, char>(p_i, p_j), std::pair<char, char>(k_col, k_row))) && (_board.path_is_legal_and_clear(std::pair<char, char>(p_i, p_j), std::pair<char, char>(k_col, k_row)))) {
     	    return true;
	  }
	}
      }
    }
  }
  //black
  else if (!white) {
    //same idea as the previous loop
    char k_col, k_row;
    for (char k_i = 'A'; k_i <= 'H'; k_i++) {
      for (char k_j = '1'; k_j <= '8'; k_j++) {
        if ((_board(std::pair<char, char>(k_i, k_j)) != NULL) && _board(std::pair<char, char>(k_i, k_j))->to_ascii() == 'k') {
          k_col = k_i;
          k_row = k_j;
        }
      }
    }
    //check if any white piece can capture black king
    for (char p_i = 'A'; p_i <= 'H'; p_i++) {
      for (char p_j = '1'; p_j <= '8'; p_j++) {
        if ((_board(std::pair<char, char>(p_i, p_j)) != NULL) && (_board(std::pair<char, char>(p_i, p_j))->is_white() != white)) {
          const Piece* start_piece = _board(std::pair<char, char>(p_i, p_j));
          if ((start_piece->legal_capture_shape(std::pair<char, char>(p_i, p_j), std::pair<char, char>(k_col, k_row))) && (_board.path_is_legal_and_clear(std::pair<char, char>(p_i, p_j), std::pair<char, char>(k_col, k_row)))) {
	    return true;
          }
        }
      }
    }
  }
  return false;
}

//almost exactly the same method as the last one
//will be used in stalemate to check
//not changing the original board but provide a new board to mess with
bool Chess::in_check( bool white, const Board& n_board) const {
  if (white) {
    char k_col, k_row;
    for (char k_i = 'A'; k_i <= 'H'; k_i++) {
      for (char k_j = '1'; k_j <= '8'; k_j++) {
        if (n_board(std::pair<char, char>(k_i, k_j)) != NULL && (n_board(std::pair<char, char>(k_i, k_j))->to_ascii() == 'K')) {
          k_col = k_i;
          k_row = k_j;
        }
      }
    }
    for (char p_i = 'A'; p_i <= 'H'; p_i++) {
      for (char p_j = '1'; p_j <= '8'; p_j++) {
        if ((n_board(std::pair<char, char>(p_i, p_j)) != NULL) && (n_board(std::pair<char, char>(p_i, p_j))->is_white() != white)) {
          const Piece* start_piece = n_board(std::pair<char, char>(p_i, p_j));
          if ((start_piece->legal_capture_shape(std::pair<char, char>(p_i, p_j), std::pair<char, char>(k_col, k_row))) && (n_board.path_is_legal_and_clear(std::pair<char, char>(p_i, p_j), std::pair<char, char>(k_col, k_row)))) {
            return true;
          }
        }
      }
    }
  }
  else if (!white) {
    char k_col, k_row;
    for (char k_i = 'A'; k_i <= 'H'; k_i++) {
      for (char k_j = '1'; k_j <= '8'; k_j++) {
        if ((n_board(std::pair<char, char>(k_i, k_j)) != NULL) && n_board(std::pair<char, char>(k_i, k_j))->to_ascii() == 'k') {
          k_col = k_i;
          k_row = k_j;
        }
      }
    }
    for (char p_i = 'A'; p_i <= 'H'; p_i++) {
      for (char p_j = '1'; p_j <= '8'; p_j++) {
        if ((n_board(std::pair<char, char>(p_i, p_j)) != NULL) && (n_board(std::pair<char, char>(p_i, p_j))->is_white() != white)) {
          const Piece* start_piece = n_board(std::pair<char, char>(p_i, p_j));
          if ((start_piece->legal_capture_shape(std::pair<char, char>(p_i, p_j), std::pair<char, char>(k_col, k_row))) && (n_board.path_is_legal_and_clear(std::pair<char, char>(p_i, p_j), std::pair<char, char>(k_col, k_row)))) {
            return true;
          }
        }
      }
    }
  }
  return false;
}

// situation of inmate 
bool Chess::in_mate( bool white ) const {
  
  if (in_check(white) && in_stalemate(white)) {
    return true;
  }
  return false;
}
      
//using in_check to help checking stalemate condition
bool Chess::in_stalemate( bool white ) const
{//copy the board so is possible to have test moves 
  Board n_board= Chess::copy_board(_board);
  //find each of the possible piece
  //looping through the entire board
  for (char p_i = 'A'; p_i <= 'H'; p_i++) {
    for (char p_j = '1'; p_j <= '8'; p_j++) {
      if ((_board(std::pair<char, char>(p_i, p_j)) != NULL) && (_board(std::pair<char, char>(p_i, p_j))->is_white() == white)) {
        const Piece* start_piece = _board(std::pair<char, char>(p_i, p_j));
	//find all the possible ending position
	for (char i = 'A'; i <= 'H'; i++) {
          for (char j = '1'; j <= '8'; j++) {
	    if ((start_piece->to_ascii() == 'N') || (start_piece->to_ascii() == 'n') || (_board.path_is_legal_and_clear(std::pair<char, char> (p_i, p_j), std::pair<char, char> (i, j)))) {
	      //if a piece exists at the destination
	      if (_board(std::pair<char, char>(i, j)) != NULL) {
		const Piece* end_piece = _board(std::pair<char, char>(i, j));
		if (end_piece->is_white() != start_piece->is_white()) {
		  if (start_piece->legal_capture_shape(std::pair<char, char>(p_i, p_j), std::pair<char, char>(i, j))) {
		    char start_piece_type = start_piece->to_ascii();
		    char end_piece_type = end_piece->to_ascii();
		    n_board.remove_piece(std::pair<char, char>(i, j));
		    n_board.add_piece(std::pair<char, char>(i, j), start_piece_type);
		    n_board.remove_piece(std::pair<char, char>(p_i, p_j));
		    if (!in_check(white, n_board)) {
		      return false;
		    }
		    n_board.add_piece(std::pair<char, char>(p_i, p_j), start_piece_type);
		    n_board.remove_piece(std::pair<char, char>(i, j));
		    n_board.add_piece(std::pair<char, char>(i, j), end_piece_type);
		  }
		}
	      }
	      //if there is not a piece
	      else {
		if (start_piece->legal_move_shape(std::pair<char, char>(p_i, p_j), std::pair<char, char>(i, j))) {
		  char piece_type = start_piece->to_ascii();
		  n_board.remove_piece(std::pair<char, char>(i, j));
		  n_board.add_piece(std::pair<char, char>(i, j), piece_type);
		  n_board.remove_piece(std::pair<char, char>(p_i, p_j));
		  if (!in_check(white, n_board)) {
		    return false;
		  }//if not resulted in check, then is not stalemate
		  
		  n_board.add_piece(std::pair<char, char>(p_i, p_j), piece_type);
		  n_board.remove_piece(std::pair<char, char>(i, j));
		}
	      }
	    }//try not to change the board after testing moves
	  }
	}
      }
    }
  }
  return true;
}


/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Chess& chess )
{
	// Write the board out and then either the character 'w' or the character 'b', depending on whose turn it is
	return os << chess.board() << ( chess.turn_white() ? 'w' : 'b' );
}

//read in the board and the turn character
std::istream& operator >> ( std::istream& is , Chess& chess )
{
  is >> chess._board;
  char turn;
  is >> turn;
  if (turn == 'w') {
    chess._turn_white = true;
  }//method defined in Chess.h to get private values;
  else if (turn == 'b') {
    chess._turn_white = false;
  }
  
  return is;
}
