#include "Queen.h"

using std::pair;

// Move shape and capture shape are the same for queen
bool Queen::legal_capture_shape(pair< char , char > start , pair< char , char > end) const {
  return Queen::legal_move_shape(start, end);
}
