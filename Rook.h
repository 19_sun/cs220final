#ifndef ROOK_H
#define ROOK_H

#include "Piece.h"

class Rook : public Piece
{
public:

        //either columns do not change or rows do not change 
	bool legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const
	{
	  if ((start.first == end.first) && (start.second == end.second)) {
	    return false;
	  }
	  if ((start.first == end.first) || (start.second == end.second)) {
	    return true;
	  }
	  return false;
	}

	bool legal_capture_shape( std::pair< char , char > start , std::pair< char , char > end ) const;

	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii( void ) const { return is_white() ? 'R' : 'r'; }

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	Rook( bool is_white ) : Piece( is_white ) {}

	friend Piece* create_piece( char );
};

#endif // ROOK_H
