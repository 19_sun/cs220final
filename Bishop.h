#ifndef BISHOP_H
#define BISHOP_H

#include "Piece.h"
#include <stdlib.h> 

class Bishop : public Piece
{
public:
	
  
  bool legal_move_shape( std::pair< char , char > start , std::pair< char , char > end ) const
  {//the bishop moves in a straight line, including diagonal
	  if (abs(end.first - start.first) != abs(end.second - start.second)) { 
	      return false;
	  }//test if is diagonal
	  if ((abs(end.first - start.first) == 0) && (abs(end.second - start.second) == 0)) {
	    return false;
	  }
	  return true;
	}

	bool legal_capture_shape(std::pair<char, char>start, std::pair<char, char>end) const;
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	char to_ascii( void ) const { return is_white() ? 'B' : 'b'; }

private:
	/////////////////////////////////////
	// DO NOT MODIFY THIS FUNCTION!!!! //
	/////////////////////////////////////
	Bishop( bool is_white ) : Piece( is_white ) {}

	friend Piece* create_piece( char );
};

#endif // BISHOP_H
