#include <iostream>
#include <utility>
#include <map>
#ifndef _WIN32
#include "Terminal.h"
#endif // !_WIN32
#include "Board.h"
#include "CreatePiece.h"

using std::string;
using std::map;
using std::pair;
using std::make_pair;
using std::cout;
using std::endl;
/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
Board::Board( void ){}

const Piece* Board::operator()( std::pair< char , char > position ) const
{
  map<pair<char,char>, Piece*>::const_iterator location;
  location = _occ.find(position);
  if(location == _occ.end()){
    return NULL;
  }
  else{
    return location->second;
  }
  return NULL;
}

bool Board::add_piece( std::pair< char , char > position , char piece_designator )
{//created correct string for testing valid input
  const string valid_piece("KkRrBbQqMmPpNn");
  const string col_names("ABCDEFGH");
  const string row_names("12345678");
  size_t found = valid_piece.find(piece_designator);
  if (found == string::npos) {
    return false;
  }
  if ((col_names.find(position.first) == string::npos) || (row_names.find(position.second) == string::npos)) {
    return false;
  }
  
	_occ[ position ] = create_piece( piece_designator );
	return true;
}

//removes a piece on the board
bool Board::remove_piece(std::pair<char, char> position)
{
  char col = position.first;
  char row =  position.second;
  // another way of checking if the input is valid
  if((row < '1' || row > '8') || (col < 'A' || col > 'H')) {
    return false;
  }

  if(_occ.count(position) == 0) {
    return false;
  }
  delete _occ[position];
  _occ.erase(position);//deleting the key
  return true;
}



// Determines whether the path is clear
// first variable is the start of path , second variable is the end of path
bool Board::path_is_legal_and_clear(std::pair<char, char> start, std::pair<char, char> end) const{
  int ver = 0, hor = 0;
  if (((start.first - end.first) * (start.second - end.second)) != 0) {
    if (abs(start.first - end.first) != abs(start.second - end.second)) {
      return false;
    }
  }
  else if ((start.first == end.first) && (start.second == end.second)) {
    return false;
  }
  if (end.first > start.first) {
    ver = 1;
    }
  else if (end.first < start.first) {
    ver = -1;
  }
  if (end.second > start.second) {
    hor = 1;
  }
  else if (end.second < start.second) {
    hor = -1;
  }
  char col = start.first;
  char row = start.second;
  while ((col != (end.first - ver) ) || (row != (end.second - hor))) {
    col += ver;
    row += hor;
    if (_occ.find(std::pair<char, char>(col, row)) != _occ.end()) {
      return false;
    }
  }
  return true;
  }
//check if both sides has 1 king
//default to false
bool Board::has_valid_kings( void ) const
{
  int count_bk = 0;
  int count_wk = 0;//initialized the count
  
  for(std::map<std::pair<char, char>, Piece*>::const_iterator it = _occ.begin(); it!=_occ.end(); it++) {
    if(it->second->to_ascii() == 'K') {
      count_wk++;
    }
    if(it->second->to_ascii() == 'k') {
      count_bk++;
    }   
  }
  if(count_bk != 1 || count_wk != 1) {
    return false;
  }//require both sides have 1 exact king
  return true;
}


void Board::display( void ) const {
  for( char r = '8'; r >= '1'; r--) {
    for(char c = 'A'; c <= 'H'; c++){
      if(_occ.find(std::pair<char, char>(c,r)) != _occ.end()){
	cout << _occ.find(std::pair<char, char>(c,r))->second->to_ascii();
      }//prints the piece at the position if found
      else{
	cout << '-';
      }//prints nothing if no piece
    }
    cout << endl;//go to the next line
  }
}

/////////////////////////////////////
// DO NOT MODIFY THIS FUNCTION!!!! //
/////////////////////////////////////
std::ostream& operator << ( std::ostream& os , const Board& board )
{
	for( char r='8' ; r>='1' ; r-- )
	{
		for( char c='A' ; c<='H' ; c++ )
		{
			const Piece* piece = board( std::pair< char , char >( c , r ) );
			if( piece ) os << piece->to_ascii();
			else        os << '-';
		}
		os << std::endl;
	}
	return os;
}

std::istream& operator >> (std::istream& is, Board& board) {
  char p;//denotes a piece
  for (char r = '8'; r >= '1'; r--){
    for ( char c = 'A'; c <= 'H'; c++) {
      is >> p;//take all the chess into the board
      if (board(std::pair<char, char>(c,r)) != NULL) {
	board.remove_piece(std::pair<char, char>(c,r));
      }
      if( p != '-') {
	board.add_piece(std::pair<char,char>(c,r), p);
      }
    }
  }
  return is;
}
